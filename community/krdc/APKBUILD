# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=krdc
pkgver=23.08.1
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/internet/krdc/"
pkgdesc="Remote Desktop Client"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="freerdp"
makedepends="
	extra-cmake-modules
	kbookmarks5-dev
	kcmutils5-dev
	kcompletion5-dev
	kconfig5-dev
	kdnssd5-dev
	kdoctools5-dev
	ki18n5-dev
	kiconthemes5-dev
	knotifications5-dev
	knotifyconfig5-dev
	knotifyconfig5-dev
	kwallet5-dev
	kwidgetsaddons5-dev
	kwindowsystem5-dev
	kxmlgui5-dev
	libssh-dev
	libvncserver-dev
	samurai
	"
_repo_url="https://invent.kde.org/network/krdc.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/krdc-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6008bf1f254d9381fea0b91b08f1e05a68e0b7e1b5cde27c84428eddd0fb6bbec6f842554bcb9bd8fea28609fdc0d1ef83289d30a2086ad2c477a0256d07c547  krdc-23.08.1.tar.xz
"
