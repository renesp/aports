# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=picard
pkgver=2.9.2
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://picard.musicbrainz.org/"
pkgdesc="Official MusicBrainz tagger"
license="GPL-2.0-or-later"
depends="
	chromaprint
	py3-dateutil
	py3-fasteners
	py3-mutagen
	py3-qt5
	py3-yaml
	"
makedepends="
	gettext
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="py3-pytest"
subpackages="$pkgname-lang $pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/metabrainz/picard/archive/release-$pkgver.tar.gz"
builddir="$srcdir/picard-release-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
7896edf8d83272d1340fde94dab33afd533d81976cced9640537c9ac37b30b9319a4fb6f180f2e2bcd5e2b25cb51a736ba9116ca3f405c10f6f6fd4935349063  picard-2.9.2.tar.gz
"
