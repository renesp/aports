# Contributor: Dmitry Romanenko <dmitry@romanenko.in>
# Maintainer: Peter Shkenev <santurysim@gmail.com>
pkgname=py3-setuptools_scm
_pkgname=setuptools-scm
pkgver=8.0.4
pkgrel=0
pkgdesc="The blessed package to manage your versions by scm tags"
url="https://github.com/pypa/setuptools_scm"
arch="noarch"
license="MIT"
depends="py3-setuptools py3-packaging"
makedepends="py3-gpep517 py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz
	typing-ext.patch
	"
builddir="$srcdir"/$_pkgname-$pkgver
options="!check" # here -> pytest -> pluggin -> here

replaces="py-setuptools_scm" # Backwards compatibility
provides="py-setuptools_scm=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	TZ=UTC .testenv/bin/python3 -m pytest -k 'not test_pip_download and not test_pyproject_support_with_git and not test_pretend_version_named_pyproject_integration'
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
1b584f20dfad115f379a97de9e96bb3b36ab00986381748c2f395facd26ce01a6556ccfbabeb8f0cf9ce8720b26ce3a00040dba02cc689701ccf1d31f13277da  setuptools-scm-8.0.4.tar.gz
5c1c161b41986b0278e1fab157e0fcedaf078c5c4e6b0974834ec5c2465f5049ee1750c47a350ba627202206e1bffcb0cd65ddb25a5186c6837496c2ce9a5fd4  typing-ext.patch
"
