# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-fitfile
_pkgorig=fitfile
pkgver=1.1.5
pkgrel=0
pkgdesc="Decode/parse Garmin's FIT format files"
url="https://github.com/tcgoetz/Fit"
arch="noarch"
license="GPL-2.0-or-later"
depends="python3"
checkdepends="
	py3-pytest-xdist
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/f/fitfile/fitfile-$pkgver.tar.gz"
builddir="$srcdir/$_pkgorig-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto
}

package() {
	python3 -m installer --destdir="$pkgdir" .dist/*.whl
}

sha512sums="
464c6a8542a4efee9ddd8f32a9772ac080935f6ff7ea2ff92e67a6f93b57477cde1da23f05e2eb5eec2c34159f200899766eb2e04060c8870248262157c114f7  py3-fitfile-1.1.5.tar.gz
"
