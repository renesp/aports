# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libkdegames
pkgver=23.08.1
pkgrel=1
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/applications/games"
pkgdesc="Common code and data for many KDE games"
license="LGPL-2.0-only AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive5-dev
	kbookmarks5-dev
	kcodecs5-dev
	kcompletion5-dev
	kconfig5-dev
	kconfigwidgets5-dev
	kcoreaddons5-dev
	kcrash5-dev
	kdbusaddons5-dev
	kdeclarative5-dev
	kdnssd5-dev
	kglobalaccel5-dev
	kguiaddons5-dev
	ki18n5-dev
	kiconthemes5-dev
	kitemviews5-dev
	kjobwidgets5-dev
	knewstuff5-dev
	kservice5-dev
	ktextwidgets5-dev
	kwidgetsaddons5-dev
	kxmlgui5-dev
	libsndfile-dev
	openal-soft-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/games/libkdegames.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkdegames-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang carddecks::noarch"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

carddecks() {
	pkgdesc="Contains all carddecks for KDE cardgames"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/share/carddecks
}

sha512sums="
c6a4c6bee7889757925ecd23e91be8567f0847ed8e86f3d7ba9317c6efa5185cc0c5dedafdabdc303b46890921e0790a1ccc0248d1e05f1882e0cca54a05ff0a  libkdegames-23.08.1.tar.xz
"
