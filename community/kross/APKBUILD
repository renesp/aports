# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kross
pkgver=5.110.0
pkgrel=1
pkgdesc="Framework for scripting KDE applications"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kcompletion5-dev
	kcoreaddons5-dev
	ki18n5-dev
	kiconthemes5-dev
	kio5-dev
	kparts5-dev
	kwidgetsaddons5-dev
	kxmlgui5-dev
	qt5-qtbase-dev
	qt5-qttools-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools5-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/kross.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kross-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9a005a5f87ae56de9f2ed7987223089800631ffd2ed41903b607cde12ab377b3e625bde2154b4f6d63f228aa29b8ddb43424842c731740e9fdc5efda9a48216a  kross-5.110.0.tar.xz
"
