# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=ktextwidgets5
pkgver=5.110.0
pkgrel=1
pkgdesc="Advanced text editing widgets"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kcompletion5-dev
	kconfig5-dev
	kconfigwidgets5-dev
	ki18n5-dev
	kiconthemes5-dev
	kservice5-dev
	kwidgetsaddons5-dev
	qt5-qtspeech-dev
	sonnet5-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/ktextwidgets.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/ktextwidgets-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
builddir="$srcdir/ktextwidgets-$pkgver"

replaces="ktextwidgets<=5.110.0-r0"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ff5b02fd481db418d3843dfba118ecd52f0d15f2e4182531cbb291d7f7628a779fd260269cd24c3a624815d36b64ff776c446ff393c903e97eda4192c6babc5c  ktextwidgets-5.110.0.tar.xz
"
