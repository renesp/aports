# Maintainer: Dominika Liberda <ja@sdomi.pl>
# Contributor: Dominika Liberda <ja@sdomi.pl>
pkgname=texlab
pkgver=5.10.0
pkgrel=0
pkgdesc="Implementation of the Language Server Protocol for LaTeX"
url="https://github.com/latex-lsp/texlab"
# limited by rust/cargo
# armhf - fails to build
arch="x86_64 armv7 aarch64 x86 ppc64le"
license="GPL-3.0-or-later"
makedepends="cargo cargo-auditable"
source="https://github.com/latex-lsp/texlab/archive/refs/tags/v$pkgver/texlab-v$pkgver.tar.gz"

# tests OOM on 32-bit
# x86_64/ppc64le tests broken with some things in /tmp
case "$CARCH" in
	x86|x86_64|ppc64le|armv7) options="!check" ;;
esac

export CARGO_PROFILE_RELEASE_PANIC="unwind"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/texlab -t "$pkgdir"/usr/bin/
}

sha512sums="
6d470a539ad969d40f31eb26bc539028b8c9bc7cba8ed3d7d586e02a6eb570e4e6be430c5f7552c0c67871cb1cce12ba832c1e9920e9c7d578aae6b6042263aa  texlab-v5.10.0.tar.gz
"
